#Bllush Slider Element

![picture](./readMeImage.png)
####A reusable Vue component in form of a slider app that can be injected into any HTML page.

##Introduction
- Vue is the FE framework of my choice. it brings a terse and fast development env. with it.
- I've set `.bllush__stories--wrapper` to an absolute element. It could be converted into a 
static + contained elemenet or even as a fixed/sticky component. All of these 'options`
are ready for a configuration panel.
-  Animation is fully scalable and also ready for a configuration panel.
- I could'nt commit to creating the Node instance (tho tempted), due to prior commitments.
 yet it would have been the same code, injected as a block.
 
 
##Install
 
 - `git clone https://yearzero@bitbucket.org/yearzero/vue-collector.git`
 - if you don't have Yarn, [just download + install it](https://yarnpkg.com/en/) and then run `npm -g i yarn`
 - `yarn install`
 - `yarn serve`
 - goto [http://localhost:8080/](http://localhost:8080/)