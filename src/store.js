import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import VueAxios from "vue-axios";
import chunk from "lodash/chunk";
Vue.use(Vuex, VueAxios, axios);

export default new Vuex.Store({
  state: {
    stories: [],
    chunkedResult: [],
    chunkSize: [],
    user: {
      actions: {
        //this will be produced via heatmap + user events (clicks, period of time hovering over, etc)
      }
    }
    //plugins:[bllushPlugin] //this is optional to sell as a plugin, connections are live via socket
    // + pickup data in backend (express + no-sql)
  },
  getters: {
    GET_STORIES: state => {
      return state.stories;
    },
    GET_CHUNKED_STORIES: state => {
      return state.chunkedResult;
    },
    GET_CHUNKED_SIZE: state => {
      return state.chunkSize;
    }
  },
  mutations: {
    SET_STORIES(state, stories) {
      state.stories = stories;
    },
    SET_CHUNKED_STORIES(state, stories) {
      state.chunkedResult = stories;
    },
    SET_CHUNK_SIZE(state, size) {
      state.chunkSize = size;
    },
    SET_USER_ACTIONS(state, { actions }) {
      state.user.actions = actions;
    }
  },
  actions: {
    SET_STORIES: async ({ commit }) => {
      const options = {
        headers: {
          "Content-Type": "application/json"
        }
      };
      let { data } = await axios.get(
        "https://api.bllush.com/sandbox/get-stories-details.json",
        options
      );
      if (data.meta.code === 200) {
        let storiesArray = data.data.stories;
        let chunkSize = 2;
        let chunkedArray = chunk(storiesArray, chunkSize);

        commit("SET_STORIES", storiesArray);
        commit("SET_CHUNKED_STORIES", chunkedArray);
        commit("SET_CHUNK_SIZE", chunkSize);
      }
    },
    SET_USER_ACTION: ({ commit }, payload) => {
      console.log({ commit }, payload);
      commit("SET_USER_ACTIONS", { payload });
    }
  }
});
